import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import "../styles/Productos.css";

function Productos(props) {
  return (
    <>
      <div className="row nextRow">
        <Container>
          {props.productes.map((producte) => (
            <div className="row nextRow">
              <div className="col">
                <div key={producte.id} className="producte">
                  <div className="row">
                    <div className="productDiv">
                      <div className="col-6 valueDiv">
                        <p>
                          {producte.nom}{" "}
                          <span style={{ paddingLeft: "2px" }}>
                            ({producte.preu} €/u)
                          </span>
                        </p>
                      </div>
                      <div className="col-6 valueDiv onRightDiv">
                        <Button
                          className="buttonAdd"
                          color="secondary"
                          onClick={() => props.addProducte(producte.id)}
                        >
                          Afegir
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </Container>
      </div>
    </>
  );
}

export default Productos;
