import React from "react";
import { Container, Row, Col, Button } from "reactstrap";

function Ticket(props) {
  return (
    <>
      <div className="row nextRow">
        <Container>
          <div className="row nextRow">
            <div className="col">
              <div key={props.producte.id} className="producte">
                <div className="row">
                  <div className="productDiv">
                    <div className="col-6 valueDiv">
                      <p>
                        {props.producte.nom}{" "}
                        <span style={{ paddingLeft: "2px" }}>
                          {props.producte.quantitat}x{props.producte.preu} €/u ={" "}
                          {props.producte.quantitat * props.producte.preu}€
                        </span>
                      </p>
                    </div>
                    <div className="col-6 valueDiv onRightDiv">
                      <Button
                        className="buttonAdd"
                        color="secondary"
                        onClick={() => props.extractProducte(props.producte.id)}
                      >
                        Treure
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </>
  );
}

export default Ticket;
