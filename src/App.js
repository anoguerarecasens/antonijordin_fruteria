import React, { useState } from "react";
import { Container, Row, Col } from "reactstrap";
import "./App.css";

import Productos from "./components/Productos";
import Ticket from "./components/Ticket";

function App() {
  const productes = [
    {
      id: 1,
      nom: "Plàtan",
      preu: 0.5,
    },
    {
      id: 2,
      nom: "Poma",
      preu: 0.8,
    },
    {
      id: 3,
      nom: "Pinya",
      preu: 5,
    },
    {
      id: 4,
      nom: "Meló",
      preu: 6,
    },
  ];

  var carrito = [
    {
      id: 1,
      nom: "Plàtan",
      preu: 0.5,
      quantitat: 0,
    },
    {
      id: 2,
      nom: "Poma",
      preu: 0.8,
      quantitat: 0,
    },
    {
      id: 3,
      nom: "Pinya",
      preu: 5,
      quantitat: 0,
    },
    {
      id: 4,
      nom: "Meló",
      preu: 6,
      quantitat: 0,
    },
  ];

  var counter1 = 0;
  var counter2 = 0;
  var counter3 = 0;
  var counter4 = 0;

  const [totalPrice, setTotalPrice] = useState();

  function addProducte(idProducte) {
    for (let i = 0; i < carrito.length; i++) {
      if (carrito[i].id === idProducte) {
        carrito[i].quantitat += 1;
      }
    }

    setTotalPrice(() => {
      var retorn = 0;
      retorn += carrito[0].preu * carrito[0].quantitat;
      retorn += carrito[1].preu * carrito[1].quantitat;
      retorn += carrito[2].preu * carrito[2].quantitat;
      retorn += carrito[3].preu * carrito[3].quantitat;
      return retorn;
    });
  }

  function extractProducte(idProducte) {}

  return (
    <>
      <Container className="container">
        <Row>
          <h1>Fruiteria</h1>
        </Row>
        <Row>
          <Col>
            <Productos productes={productes} addProducte={addProducte} />
          </Col>
          <Col></Col>
          <Col>
            {carrito.map((producte) =>
              producte.quantitat > 0 ? (
                <Ticket producte={producte} extractProducte={extractProducte} />
              ) : (
                <div></div>
              )
            )}
            <div className="totalDiv">Total: {totalPrice}</div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default App;
